package test.java;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.jupiter.api.AfterAll;

import ednoem.test.Handler;

interface FastTests {
}

@Category({ FastTests.class })
public class HandlerTest {
	private static Handler HANDLER;

	static {
		HANDLER = new Handler();
	}

	@Test(expected = ArithmeticException.class)
	public void testInnerSize() {
		HandlerTest.HANDLER.parition(new int[2], 3);
	}

	@Test(expected = ArithmeticException.class)
	public void testArraySize() {
		HandlerTest.HANDLER.parition(new int[0], 0);
	}

	@Test
	public void testInnerArraysSize() {
		int[][] output = HANDLER.parition(new int[] { 52, 4, 8, 9, 4, 55 }, 2);
		for (int[] outer : output) {
			assertEquals("The size should be exactly 2 !", outer.length, 2);
		}
	}

	@Test
	public void testArraySorted() {
		int[] input = new int[] { 52, 4, 8, 9, 4, 55 };
		int[][] output = HANDLER.parition(input, 2);
		int[] container = new int[input.length];

		int index = 0;
		for (int[] outer : output) {
			for (int inner : outer) {
				container[index++] = inner;
			}
		}
		Arrays.sort(input);
		assertArrayEquals("Arrays should be equal & sorted !", input, container);
	}
	
	@AfterAll
	public void clean() {
		HANDLER = null;
	}

}
