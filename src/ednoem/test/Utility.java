package ednoem.test;

import java.util.Arrays;

/**
 * @author ROUINEB Hamza Note that is possible to implement this function in a
 *         static way (only on java 8, static function interface), e.g : public
 *         static int[][] partition(int[] input, int size) or, even make a class
 *         function (static). But doing like this, gives you me flexibility, and
 *         of course you can override it if you wish so. Nevertheless, the
 *         classes that will inherit from Handler, will not get the function
 *         partition as you may think.
 */

public interface Utility {

	public final static String ERROR_EMPTY = "Cannot put empty array";
	public final static String ERROR_SIZE = "Cannot fix the size to a value greater than the array's size";

	/**
	 * This is a default implementation of a simple sorting function, that takes
	 * two inputs into account : the first one is an array not necessarily
	 * sorted, and the second one is the size of inner arrays
	 * 
	 * @param input
	 *            the given integer array, user's input
	 * @param size
	 *            the size of inner arrays that will be created
	 * 
	 * @return a sorted array containing inner arrays having the 'size' length.
	 */
	public default int[][] parition(int[] input, int size) throws ArithmeticException {
		// the input should not be empty !
		if (input.length == 0)
			throw new ArithmeticException(ERROR_EMPTY);

		if (size > input.length)
			throw new ArithmeticException(ERROR_SIZE);

		// size should be a positive greater than 0
		int default_size = (size == 0) ? 2 : size;

		// make sure that the array is sorted !
		Arrays.sort(input);

		// number of rows
		int rows = (input.length / size) + ((input.length % default_size != 0) ? 1 : 0);

		// the output array to be returned
		int[][] output = new int[rows][];
		int count = input.length;

		for (int i = 0; count != 0; ++i) {
			if (count - default_size >= 0) {
				count -= default_size;
				output[i] = new int[default_size];
			} else if (count > 0) {
				output[i] = new int[count];
				count = 0;
			}

			for (int j = 0; j < output[i].length; ++j) {
				output[i][j] = input[default_size * i + j];
			}
		}
		return output;
	}

}
